package com.commissionPayment;

/**
 * Java packages to be used in connecting to the database.
 */

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.InputMismatchException;
import java.util.Scanner;

public class Sales {

    /**
     * Declare variables that will be used by various methods during the CRUD process.
     *
     * Variables should be same as column names in the database table.
     */

    private int sales_id;
    private double firstRate = 0.1; //Commissions rate.
    private double secondRate = 0.2; //Commissions rate.
    private double thirdRate = 0.3; //Commissions rate.
    private double amt;
    private double commissions = 0;

    public double getAmount() {

        /**
         * Define a getAmount method that accepts employee weekly sales
         */
        Scanner value = new Scanner(System.in); //value object that enables the user to input values to the console.

        amt = 0; //initialize amount to 0

        try {
            System.out.print("Enter Weekly sales amount: ");
            amt = value.nextInt();
        } catch (IllegalArgumentException | InputMismatchException e) {
            e.printStackTrace();
        }

        System.out.printf("Please wait as your Commission is processed...\n");

        return amt;

    }

    public double calcCommission(double amt) {

        /**
         * Method to calculate Employee commissions
         */

        if (amt == 20000) {
            //No commission payable
            System.out.println("The income commission is: " + 0);
        } else if (amt >= 40000 && amt <= 59999) {
            //Use the first commission conversion rate if the employee has made sales above 20000 but below 40000.
            System.out.println("The income commission is: " + String.format("%.2f", (amt * firstRate)));
        } else if (amt >= 60000 && amt <= 80000){
            //Use the second commission conversion rate if the employee has made sales above 40000 but below 60000.
            System.out.println("The income commission is: " + String.format("%.2f", (amt * secondRate)));
        } else {
            //Use the third commission conversion rate if the employee has made sales above 60000.
            System.out.println("The income commission is: " + String.format("%.2f", (amt * thirdRate)));
        }

        return commissions;

    }

    public void add(Connection con) {

        /**
         * Insert method.
         *
         * Wrap in a try {} catch {] function to capture any errors when inserting values to the database.
         */

        try {

            //insertQuery variable that accepts a SQL query to be executed.

            String insertQuery = " insert into sales (amount, commissions)"
                    + " values (?, ?)";


            PreparedStatement insert = con.prepareStatement(insertQuery); //pass the insertQuery as a parameter.

            insert.setDouble(1, amt);
            insert.setDouble(2, commissions);

            insert.executeUpdate(); //query the database and add the values to specific columns.

            System.out.println(" Transaction Successful ");
        }catch (Exception e) {
            //catch any exceptions.

            System.err.println("Got an exception!");
            System.err.println(e.getMessage());
        }
    }

    public void delete(Connection con) {

        try {

            Scanner input = new Scanner(System.in);

            // Enter the id in the sales table to fetch and delete the row.
            System.out.println("Enter Sales ID...");
            sales_id= input.nextInt();

            String deleteQuery = "delete from sales where sales_id = ?"; //query the database.

            PreparedStatement delete = con.prepareStatement(deleteQuery); //pass in the deleteQuery variable.
            delete.setInt (1, sales_id);

            delete.executeUpdate(); //execute the query.
            System.out.println("Sale record deleted");
        }catch(Exception e) {
            System.err.println("Got an exception!");
            System.err.println(e.getMessage());
        }
    }

}
