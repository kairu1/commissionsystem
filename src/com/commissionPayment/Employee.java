package com.commissionPayment;

/**
 * Packages to be used in connecting to the database.
 */

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Scanner;

public class Employee {

    /**
     * Declare variables that will be used by various methods during the CRUD process.
     *
     * Variables should be same as column names in the database table.
     */
    private String fname;
    private String lname;
    private int phone_number;

    public void show(Connection con) {

        /**
         * Read method.
         *
         * Wrap in a try {} catch {] function to capture any errors when fetching values from the database.
         *
         */
        try {

            /**
             * Declare a selectQuery variable that accepts a SQL select statement.
             * Create a select object using PreparedStatement.
             * Create an rs object to execute the statement passed in to the selectQuery and fetch the results.
             */
            String selectQuery = "select * from employee";

            PreparedStatement select = con.prepareStatement(selectQuery);
            ResultSet rs = select.executeQuery(); //Get the results and place it

            while (rs.next()) {
                System.out.println(rs.getInt(1) + " " + rs.getString(2)
                        + " " + rs.getString(3) + " " + rs.getInt(4)
                        + " " + rs.getInt(5)
                );
            }
        } catch (Exception e) {
            //Capture any errors.

            System.err.println("Got an exception!");
            System.err.println(e.getMessage());
        }
    }

    public void add(Connection con) {

        /**
         * Insert method.
         *
         * Wrap in a try {} catch {] function to capture any errors when inserting values to the database.
         */

        try {

            Scanner input = new Scanner(System.in); //input object to accept user input from the console.

            /**
             * input.nextLine() is a method accessed after creating the input object using Scanner
             */
            System.out.println("Enter your First Name...");
            fname = input.nextLine();
            System.out.println("Enter your Last Name...");
            lname = input.nextLine();
            System.out.println("Enter Phone Number...");
            phone_number= input.nextInt();

            String insertQuery = " insert into employee (fname, lname, phone_number)"
                    + " values (?, ?, ?)";


            /**
             * Create an insert object to access methods in the PreparedStatement interface.
             * pass the insertQuery to the prepareStatement to query the database.
             */
            PreparedStatement insert = con.prepareStatement(insertQuery);
            insert.setString(1, fname);
            insert.setString(2, lname);
            insert.setInt(3, phone_number);

            insert.executeUpdate(); //execute the query and update the database with the inserted values.
            System.out.println(" Employee records inserted");
        }catch (Exception e) {
            System.err.println("Got an exception!");
            System.err.println(e.getMessage());
        }
    }

    public void update(Connection con) {

        /**
         * Update method to alter database values.
         * Wrap the code in a try... catch{] block to capture any errors.
         */

        try {

            Scanner input = new Scanner(System.in); // input object.

            System.out.println("Update your First Name...");
            fname = input.nextLine();
            System.out.println("Enter Phone Number...");
            phone_number= input.nextInt();

            String updateQuery = "update employee set fname = ? where phone_number = ?"; //SQL update Query

            PreparedStatement update = con.prepareStatement(updateQuery);
            update.setString (1, fname);
            update.setInt (2, phone_number);

            update.executeUpdate(); //Execute the query and update the database with the new values.
            System.out.println("Employee records updated");
        }catch (Exception e) {
            // Capture any exceptions when calling the method.

            System.err.println("Got an exception!");
            System.err.println(e.getMessage());
        }
    }

    public void delete(Connection con) {

        /**
         * Delete Method.
         */
        try {

            Scanner input = new Scanner(System.in); // input object.

            System.out.println("Enter Phone Number..."); //Enter the field column
            phone_number= input.nextInt();

            String deleteQuery = "delete from employee where phone_number = ?"; //SQL Delete Query.

            PreparedStatement delete = con.prepareStatement(deleteQuery);
            delete.setInt (1, phone_number);

            delete.executeUpdate(); //execute the delete query passed in the deleteQuery variable.
            System.out.println("Employee record deleted");
        }catch(Exception e) {
            // catch any exceptions.

            System.err.println("Got an exception!");
            System.err.println(e.getMessage());
        }
    }
}
