package com.commissionPayment;
/**
 * Packages to be used in Connecting to the Database.
 */

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Scanner;

public class Main {

    /**
     * Create variables that hold database login credentials (USERNAME, PASSWORD)
     *
     * Create a connection variable (CONN) that holds the address to the database server.
     */

    private static  final String USERNAME = "root";
    private static  final String PASSWORD = "";
    private static final String CONN = "jdbc:mysql://localhost:3306/commission_system";


    public static void main (String [] args) throws SQLException {

        //connect to the database driver using the forName() method.

        try {
            Class.forName("com.mysql.jdbc.Driver");

            //create a Connection object "con"

            Connection con = null;

            //use the getConnection() method in the DriverManager interface to connect

            System.out.println("Connecting to database...\n");

            con = DriverManager.getConnection(CONN, USERNAME, PASSWORD);

            System.out.println("Connected Successfully\n"); //Database message for successful connection.

            /**
             * Create an Employee object (emp) and call its methods.
             *
             * Create a scanner object to allow the user to input values through the console.
             *
             * Use a switch case statement to enable the user to choose which action they want.
             */
            Employee emp = new Employee();
            Scanner choice = new Scanner(System.in);

            System.out.println("1 >>> Create a new Employee.");
            System.out.println("2 >>> Update Employee records.");
            System.out.println("3 >>> Delete Employee.");
            System.out.println("4 >>> Show Employee records.");
            System.out.print("Please Select a number from 1 - 4...");
            int choose = choice.nextInt();

            switch(choose) {
                case 1 :
                    emp.add(con);
                    break;
                case 2 :
                    emp.update(con);
                    break;
                case 3 :
                    emp.delete(con);
                    break;
                case 4 :
                    emp.show(con);
                    break;
                default :
                    System.out.println("Invalid choice...\n");
            }

            System.out.println();

            /**
             * Create a Sales object (amount) and call its methods.
             *
             * Create a scanner object to allow the user to input values through the console.
             *
             * Use a switch case statement to enable the user to choose which action they want.
             */

            Sales amount = new Sales();
            Scanner sales = new Scanner(System.in);

            System.out.println("1 >>> Enter weekly sales.");
            System.out.println("2 >>> Delete sales record.");
            System.out.println("Please Select a number between 1 & 2.");

            int chooseNumber = sales.nextInt(); //variable to be used by the switch case statement.

            switch(chooseNumber) {
                case 1 :

                    double x = amount.getAmount();
                    amount.calcCommission(x);
                    amount.add(con);
                    break;
                case 2 :
                    amount.delete(con);
                    break;
                default :
                    System.out.println("Invalid choice...");
            }


            con.close(); //End connection to the database.

        }catch (Exception e){

            /**
             * Catch any errors during the whole process of connecting and adding data to the database.
             */
            System.err.println("Got an exception!");
            System.err.println(e.getMessage());
        }

    }
}
